import type Order from "./Order";

export default interface Employee {
  id?: number;
  name: string;
  // address: string;
  // tel: string;
  // email: string;
  position: string;
  hourly_wage: number;
  orders?: Order;
  start_date?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
