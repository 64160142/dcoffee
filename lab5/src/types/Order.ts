import type OrderItem from "./OrderItem";

export default interface Order {
  id?: number;
  employeeId: number;
  queue?: number;
  order_date_time?: Date;
  discount?: number;
  total?: number;
  received?: number;
  change?: number;
  payment?: string;
  amount?: number;
  tel?: string;
  // store: Store; // Store Id
  // employee: Employee; // Employee Id
  // customer: Customer; // Customer Id
  orderItems?: OrderItem[];
  updatedDate?: Date;
  deletedDate?: Date;
}
