export default interface Item {
  name: string;
  price: string;
  quantity: number;
}
