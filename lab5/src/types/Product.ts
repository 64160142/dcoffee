import type Category from "./Category";

export default interface Product {
  id: number;
  name: string;
  type?: string;
  size?: string;
  price: number;
  image?: string;
  catagoryId: number;
  category?: Category;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
