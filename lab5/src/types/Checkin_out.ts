import type Employee from "./Employee";

export default interface Checkin_out {
  id?: number;
  date: Date;
  time_in: Date;
  time_out: Date;
  total_hour: number;
  employee?: Employee;
  // summary_salary: SummarySalary;
}
