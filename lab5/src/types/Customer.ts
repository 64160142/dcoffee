export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  point: number;
  start_date?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
