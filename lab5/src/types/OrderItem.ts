import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem {
  id?: number;
  name?: string;
  amount?: number;
  price?: number;
  total?: number;
  product?: Product; // Product Id
  order?: Order;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
