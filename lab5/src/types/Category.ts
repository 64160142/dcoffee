export default interface Category {
  id?: number;
  name: string;
  start_date?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
