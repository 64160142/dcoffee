export default interface Checkin_out {
  id?: number;
  name: string;
  min_quantity: number;
  quantity: number;
  unit: string;
  price_per_unit: number;
}
