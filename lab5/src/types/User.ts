import type Employee from "./Employee";

export default interface User {
  id?: number;
  login?: string;
  name: string;
  password: string;
  role: string;
  employee?: Employee;
}
