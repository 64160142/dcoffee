import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/home",
      name: "home",
      component: () => import("../views/home/HomeView.vue"),
    },
    {
      path: "/pos",
      name: "pos",
      component: () => import("../views/Pos/ProductContainerComponent.vue"),
    },
    {
      path: "/posi",
      name: "posi",
      component: () => import("../views/Pos/PosView.vue"),
    },
    {
      path: "/user",
      name: "user",
      component: () => import("../views/user/UserView.vue"),
    },
    {
      path: "/employee",
      name: "employee",
      component: () => import("../views/employee/EmployeeView.vue"),
    },
    {
      path: "/customer",
      name: "customer",
      component: () => import("../views/customer/CustomerView.vue"),
    },
    {
      path: "/product",
      name: "product",
      component: () => import("../views/products/ProductView.vue"),
    },
    {
      path: "/products_page",
      name: "products_page",
      component: () => import("../views/ProductPaginationView.vue"),
    },
    {
      path: "/material",
      name: "material",
      component: () => import("../views/material/MaterialView.vue"),
    },
    {
      path: "/historysell",
      name: "historysell",
      component: () => import("../views/products/HistorySell.vue"),
    },
    {
      path: "/checkin_out",
      name: "checkin_out",
      component: () => import("../views/checkin_out/Checkin_outView.vue"),
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: "/login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
