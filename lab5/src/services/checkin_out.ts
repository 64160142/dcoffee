import type Checkin_out from "@/types/Checkin_out";
import http from "./axios";
function getCategories() {
  return http.get("/checkin-out");
}

function saveCheckin_out(checkin_out: Checkin_out) {
  return http.post("/checkin-out", checkin_out);
}

function updateCheckin_out(id: number, checkin_out: Checkin_out) {
  return http.patch(`/checkin-out/${id}`, checkin_out);
}

function deleteCheckin_out(id: number) {
  return http.delete(`/checkin-out/${id}`);
}

export default {
  getCategories,
  saveCheckin_out,
  updateCheckin_out,
  deleteCheckin_out,
};
