import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomers() {
  return http.get("/customers");
}

function getCustomersProcedure() {
  return http.get("/reports/customer");
}

function delCustomersProcedure() {
  return http.get("/reports/delCus");
}

function getCustomersByTel(tel: string) {
  return http.get(`/customers/tel/${tel}`);
}
function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

export default {
  getCustomers,
  saveCustomer,
  updateCustomer,
  deleteCustomer,
  getCustomersByTel,
  getCustomersProcedure,
  delCustomersProcedure,
};
