import type Category from "@/types/Category";
import http from "./axios";
function getCategories() {
  return http.get("/catagorys");
}

function saveCategory(category: Category) {
  return http.post("/catagorys", category);
}

function updateCategory(id: number, category: Category) {
  return http.patch(`/catagorys/${id}`, category);
}

function deleteCategory(id: number) {
  return http.delete(`/catagorys/${id}`);
}

export default { getCategories, saveCategory, updateCategory, deleteCategory };
