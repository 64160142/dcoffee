import type Product from "@/types/Product";
import http from "./axios";

function getProductsByCategory(category: number) {
  return http.get(`/products/catagory/${category}`);
}

function getProducts(params: any) {
  return http.get("/products", { params: params });
}

function saveProducts(product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("type", `${product.type}`);
  formData.append("size", `${product.size}`);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]);
  formData.append("catagoryId", `${product.catagoryId}`);
  return http.post("/products", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

// categoryId

function updateProducts(id: number, product: Product & { files: File[] }) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("type", `${product.type}`);
  formData.append("size", `${product.size}`);
  formData.append("price", `${product.price}`);
  if (product.files) {
    formData.append("file", product.files[0]);
  }
  return http.patch(`/products/${id}`, formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

function delProducts(id: number) {
  console.log("del");
  return http.delete(`/products/${id}`);
}
// function deleteCustomer(id: number) {
//   return http.delete(`/customers/${id}`);
// }

export default {
  getProducts,
  saveProducts,
  updateProducts,
  delProducts,
  getProductsByCategory,
};
