import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import { useLoadingStore } from "./loading";
import productService from "@/services/product";
import { useMessageStore } from "./message";
export const useProductPageStore = defineStore("productPage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const page = ref(2);
  const take = ref(2);
  const keyword = ref("");
  const products = ref<Product[]>([]);
  const lastPage = ref(0);
  watch(keyword, async (newkeyword, oldkeyword) => {
    await getProducts();
  });
  watch(page, async (newPage, oldPage) => {
    await getProducts();
  });
  watch(lastPage, async (newLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1;
    }
  });
  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts({
        page: page.value,
        take: take.value,
        keyword: keyword.value,
      });
      products.value = res.data.data;
      lastPage.value = res.data.lastPage;
      console.log(products.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  return { page, take, keyword, products, getProducts, lastPage };
});
