import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const showMessage = (msg: string) => {
    message.value = msg;
    isShow.value = true;
  };
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  };
  function showError(text: string) {
    message.value = text;
    isShow.value = true;
  }

  return { isShow, message, showMessage, closeMessage, showError };
});
