import { ref, watch } from "vue";
import { defineStore } from "pinia";
import axios from "axios";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    name: "",
    address: "",
    tel: "",
    email: "",
    position: "",
    hourly_wage: 0,
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedEmployee.value = {
        name: "",
        address: "",
        tel: "",
        email: "",
        position: "",
        hourly_wage: 0,
      };
    }
  });

  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get("http://localhost:3000/employees");
      employees.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }
      dialog.value = false;
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }

  async function getEmployeeId(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployeeId(id);
      await getEmployees();
      console.log(res);
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    daleteEmployee,
    getEmployeeId,
  };
});
