// import { ref, computed } from "vue";
// import { defineStore } from "pinia";
// import { useUserStore } from "./user";
// import { useMessageStore } from "./message";
// import router from "@/router";
// export const useLoginStore = defineStore("login", () => {
//   const userStore = useUserStore();
//   const messageStore = useMessageStore();
//   const loginName = ref("");
//   const passWord = ref("");
//   const isLogin = computed(() => {
//     return loginName.value !== "";
//   });
//   const login = (userName: string, password: string): void => {
//     if (userStore.login(userName, password)) {
//       loginName.value = userName;
//       passWord.value = password;
//       localStorage.setItem("loginName", userName);
//       localStorage.setItem("password", password);
//       router.push("/");
//     } else {
//       messageStore.showMessage("Login ไม่ถูกต้อง");
//     }
//   };

//   const logout = () => {
//     loginName.value = "";
//     localStorage.removeItem("loginName");
//   };
//   const loadData = () => {
//     loginName.value = localStorage.getItem("loginName") || "";
//   };

//   return { loginName, isLogin, login, logout, loadData };
// });
