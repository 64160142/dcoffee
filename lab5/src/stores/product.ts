import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const keyword = ref("");
  const products = ref<Product[]>([]);
  const category = ref(1);
  const editedProduct = ref<Product & { files: File[] }>({
    id: 0,
    name: "",
    type: "",
    size: "",
    price: 0,
    catagoryId: 1,
    image: "",
    files: [],
  });
  watch(keyword, async (newPage, oldPage) => {
    await getProducts();
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedProduct.value = {
        id: 0,
        name: "",
        type: "",
        size: "",
        price: 0,
        catagoryId: 1,
        image: "",
        files: [],
      };
    }
  });
  watch(category, async (newCategory) => {
    await getProductsByCategory(newCategory);
  });
  async function getProductsByCategory(category: number) {
    loadingStore.isLoading = true;
    try {
      console.log(category);
      const res = await productService.getProductsByCategory(category);
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts({ keyword: keyword.value });
      products.value = res.data.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProducts(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProducts(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.delProducts(id);
      console.log("del");
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    dialog,
    products,
    getProducts,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    category,
    getProductsByCategory,
    keyword,
  };
});
