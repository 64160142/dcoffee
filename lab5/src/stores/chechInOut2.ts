import { ref } from "vue";
import { defineStore } from "pinia";

export const useCheckInOut2 = defineStore("dialog", () => {
  const dialogCheckInOut = ref(false);

  return { dialogCheckInOut };
});
