import { ref } from "vue";
import { defineStore } from "pinia";

export const useCashStore = defineStore("cash", () => {
  const cash = ref(false);

  return { cash };
});
