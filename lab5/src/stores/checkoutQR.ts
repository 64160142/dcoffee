import { ref } from "vue";
import { defineStore } from "pinia";

export const useCheckoutQR = defineStore("dialog", () => {
  const dialogQR = ref(false);

  return { dialogQR };
});
