import { ref, watch } from "vue";
import { defineStore } from "pinia";
import axios from "axios";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "", point: 0 };
    }
  });

  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get("http://localhost:3000/customer");
      customers.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function getCustomersProcedure() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomersProcedure();
      customers.value = res.data;
      console.log(res.data);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function delCustomersProcedure() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.delCustomersProcedure();
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
    const load = getCustomersProcedure();
  }

  async function addPointsByTel(tel: string, pointsToAdd: number) {
    try {
      const customerRes = await customerService.getCustomersByTel(tel);
      if (customerRes.data.length > 0) {
        const customer = customerRes.data[0];

        // Update the customer's points
        customer.point += pointsToAdd;

        // Save the updated customer data
        await customerService.updateCustomer(customer.id, customer);
        messageStore.showSuccess(
          `เพิ่ม ${pointsToAdd} คะแนนให้กับ ${tel} สำเร็จ`
        );
      } else {
        messageStore.showError("ไม่พบข้อมูลลูกค้า");
      }
    } catch (e) {
      messageStore.showError("ไม่สามารถเพิ่มคะแนนให้กับลูกค้าได้");
      console.log(e);
    }
  }

  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }
      dialog.value = false;
      await getCustomersProcedure();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteCustomer(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  async function getCustomerById(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get(`http://localhost:3000/customers/${id}`);
      const customer = res.data;
      console.log(customer);
    } catch (e) {
      messageStore.showError(`ไม่สามารถดึงข้อมูล Customer ด้วย id ${id} ได้`);
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }

  return {
    customers,
    getCustomers,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    daleteCustomer,
    getCustomerById,
    addPointsByTel,
    getCustomersProcedure,
    delCustomersProcedure,
  };
});
