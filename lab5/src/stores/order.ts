import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/services/order";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import axios from "axios";
import type Product from "@/types/Product";
import { useAuthStore } from "./auth";
import customerService from "@/services/customer";
// import { useCheckoutStore } from "./checkout";
// import CheckOutQRDialogVue from "@/views/Pos/CheckOutQRDialog.vue";
// import { useCustomerStore } from "./customer";

export const useOrderStore = defineStore("order", () => {
  const loadingStore = useLoadingStore();
  // const checkoutQR =
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const num = ref(0);
  const telephone = ref("");
  const editedOrder = ref<Order>();
  const orders = ref<Order[]>([]);
  const totalprice = ref(0);
  const discount = ref(0);
  const usepoint = ref(false);
  const receivedQR = ref(0);
  const orderList = ref<{ product: Product; amount: number; sum: number }[]>(
    []
  );
  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, sum: 1 * item.price });
  }
  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  });
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  async function getOrders() {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get("http://localhost:3000/orders");
      orders.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  async function openOrder() {
    loadingStore.isLoading = true;
    console.log(telephone.value, discount.value);
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.product.id,
          amount: item.amount,
        }
    );

    const order = {
      tel: telephone.value,
      employeeId: user.id,
      orderItems: orderItems,
      received: num.value,
      payment: "cash",
      telephone: telephone.value,
      discount: discount.value,
    };
    console.log(order);
    try {
      // console.log(telephone.value);
      const res = await orderService.saveOrder(order);

      // Fetch the customer's data by the telephone number
      const customerRes = await customerService.getCustomersByTel(
        telephone.value
      );
      console.log(telephone.value);
      if (customerRes.data.length > 0) {
        const customer = customerRes.data[0];
        console.log(discount.value);
        // Update the customer's points

        //const usepoint = ref(false);
        if (discount.value) {
          customer.point -= 10;
          console.log("use point -10");
        } else {
          customer.point += 2;
          console.log("no use point +2");
        }

        // Save the updated customer data
        await customerService.updateCustomer(customer.id, customer);
      } else {
        messageStore.showError("ไม่พบข้อมูลลูกค้า");
      }

      dialog.value = false;
      await getOrders();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }

    discount.value = 0;
    telephone.value = "";
    usepoint.value = false;
    loadingStore.isLoading = false;
    // clearOrder();
  }

  async function testGetCustomersByTel(tel: string) {
    try {
      const res = await customerService.getCustomersByTel(tel);
      if (res.data.length > 0) {
        const customer = res.data[0];
        console.log(
          "Customer name:",
          customer.name,
          "Customer points:",
          customer.point
        );
      } else {
        console.log("No customer found with this telephone number.");
      }
    } catch (error) {
      console.log("Error fetching customer by tel:", error);
    }
  }

  async function GetPointByTel(tel: string): Promise<number> {
    try {
      const res = await customerService.getCustomersByTel(tel);
      if (res.data.length > 0) {
        const customer = res.data[0];
        console.log("Customer points:", customer.point);
        return customer.point;
      } else {
        console.log("No customer found with this telephone number.");
        return 0;
      }
    } catch (error) {
      console.log("Error fetching customer by tel:", error);
      return 0;
    }
  }

  async function openOrderQR(telephone: String) {
    console.log(telephone, discount.value);
    loadingStore.isLoading = true;
    const user: { id: number } = authStore.getUser();
    const orderItems = orderList.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.product.id,
          amount: item.amount,
        }
    );
    const cash = receivedQR.value;

    const order = {
      employeeId: user.id,
      orderItems: orderItems,
      received: cash,
      payment: "QR",
      telephone: telephone,
      discount: discount.value,
    };
    console.log(order);
    try {
      const res = await orderService.saveOrder(order);

      // Fetch the customer's data by the telephone number
      const customerRes = await customerService.getCustomersByTel(telephone);
      console.log(telephone);
      if (customerRes.data.length > 0) {
        const customer = customerRes.data[0];
        console.log(discount.value);
        // Update the customer's points

        //const usepoint = ref(false);
        if (discount.value) {
          customer.point -= 10;
          console.log("use point -10");
        }
        if (sumPrice.value - totalprice.value === 0) {
          customer.point += 2;
          console.log("no use point +2");
        }

        // Save the updated customer data
        await customerService.updateCustomer(customer.id, customer);
      } else {
        messageStore.showError("ไม่พบข้อมูลลูกค้า");
      }
      dialog.value = false;
      await getOrders();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Order ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
    // clearOrder();
  }

  async function deleteOrder(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await orderService.deleteOrder(id);
      await getOrders();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Order ได้");
    }
    loadingStore.isLoading = false;
  }
  function editOrder(order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(order));
    dialog.value = true;
  }
  function clearOrder() {
    orderList.value = [];
  }
  return {
    orders,
    deleteOrder,
    dialog,
    editedOrder,
    //saveOrder,
    editOrder,
    getOrders,
    isTable,
    addProduct,
    sumAmount,
    sumPrice,
    orderList,
    deleteProduct,
    openOrder,
    num,
    clearOrder,
    openOrderQR,
    telephone,
    testGetCustomersByTel,
    totalprice,
    discount,
    usepoint,
    GetPointByTel,
    receivedQR,
  };
});
