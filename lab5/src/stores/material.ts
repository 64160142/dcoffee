import { ref, watch } from "vue";
import { defineStore } from "pinia";
import axios from "axios";
import type Material from "@/types/Material";
import materialService from "@/services/material";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useMaterialStore = defineStore("Material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const materials = ref<Material[]>([]);
  const lowmaterialcount = ref(0);
  const editedMaterial = ref<Material>({
    name: "",
    min_quantity: 0,
    quantity: 0,
    unit: "",
    price_per_unit: 0,
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedMaterial.value = {
        name: "",
        min_quantity: 0,
        quantity: 0,
        unit: "",
        price_per_unit: 0,
      };
    }
  });

  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get("http://localhost:3000/materials");
      materials.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Material ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterial() {
    loadingStore.isLoading = true;
    try {
      if (editedMaterial.value.id) {
        const res = await materialService.updateMaterial(
          editedMaterial.value.id,
          editedMaterial.value
        );
      } else {
        const res = await materialService.saveMaterial(editedMaterial.value);
      }
      dialog.value = false;
      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Material ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteMaterial(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await materialService.deleteMaterial(id);
      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Material ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
  }

  function countMaterialsUnderMinimum(materials: Material[]): number {
    let count = 0;
    for (const material of materials) {
      if (material.quantity < material.min_quantity) {
        count++;
      }
    }
    console.log(count);
    lowmaterialcount.value = count;
    console.log(lowmaterialcount.value);
    return count;
  }

  return {
    materials,
    getMaterials,
    dialog,
    editedMaterial,
    saveMaterial,
    editMaterial,
    daleteMaterial,
    countMaterialsUnderMinimum,
    lowmaterialcount,
  };
});
