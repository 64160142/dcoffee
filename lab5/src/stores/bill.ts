import { ref } from "vue";
import { defineStore } from "pinia";

export const useBillStore = defineStore("dialog", () => {
  const dialogBill = ref(false);

  return { dialogBill };
});
