import { ref, watch } from "vue";
import { defineStore } from "pinia";
import axios from "axios";
import type Checkin_out from "@/types/Checkin_out";
import checkin_outService from "@/services/checkin_out";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useCheckin_outStore = defineStore("Checkin_out", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const checkin_outs = ref<Checkin_out[]>([]);
  const editedCheckin_out = ref<Checkin_out>({
    date: new Date(),
    time_in: new Date(),
    time_out: new Date(),
    total_hour: 0,
  });

  watch(dialog, (newDialog) => {
    if (!newDialog) {
      editedCheckin_out.value = {
        date: new Date(),
        time_in: new Date(),
        time_out: new Date(),
        total_hour: 0,
      };
    }
  });

  async function getCheckin_outs() {
    loadingStore.isLoading = true;
    try {
      const res = await axios.get("http://localhost:3000/checkin-out");
      checkin_outs.value = res.data;
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล Checkin_out ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveCheckin_out() {
    loadingStore.isLoading = true;
    try {
      if (editedCheckin_out.value.id) {
        const res = await checkin_outService.updateCheckin_out(
          editedCheckin_out.value.id,
          editedCheckin_out.value
        );
      } else {
        const res = await checkin_outService.saveCheckin_out(
          editedCheckin_out.value
        );
      }
      dialog.value = false;
      await getCheckin_outs();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Checkin_out ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function daleteCheckin_out(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await checkin_outService.deleteCheckin_out(id);
      await getCheckin_outs();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล Checkin_out ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  function editCheckin_out(checkin_out: Checkin_out) {
    editedCheckin_out.value = JSON.parse(JSON.stringify(checkin_out));
    dialog.value = true;
  }
  return {
    checkin_outs,
    getCheckin_outs,
    dialog,
    editedCheckin_out,
    saveCheckin_out,
    editCheckin_out,
    daleteCheckin_out,
  };
});
