import type User from "@/types/User";
import { defineStore } from "pinia";
import { ref, watch } from "vue";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import userService from "@/services/user";

export const useUserStore = defineStore("user", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const users = ref<User[]>([]);

  const editedUser = ref<User>({
    name: "",
    login: "",
    password: "",
    role: "",
  });

  watch(dialog, (newDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = {
        name: "",
        login: "",
        password: "",
        role: "",
      };
    }
  });

  // const login = (loginName: string,password:string):boolean=>{
  //   const index = users.value.findIndex((item) => item.login === loginName);
  //   if(index>=0){
  //     const user = users.value[index];
  //     if (user.password === password){
  //       return true;
  //     }
  //     return false;
  //   }
  //   return false;
  // }

  const getUsers = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(users.value);
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const getUserRoles = async () => {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(users.value);
      // กำหนดค่า role จากข้อมูลผู้ใช้ที่มีชื่อเท่ากับ user.name
    } catch (e) {
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const saveUser = async () => {
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = userService.saveUser(editedUser.value);
      }
      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  };

  const deleteUser = (id: number): void => {
    loadingStore.isLoading = true;
    try {
      const index = users.value.findIndex((item) => item.id === id);
      users.value.splice(index, 1);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ User ได้");
    }
    loadingStore.isLoading = false;
  };

  const editUser = (user: User) => {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  };
  const clear = () => {
    editedUser.value = {
      id: -1,
      login: "",
      name: "",
      password: "",
      role: "",
    };
  };
  return {
    users,
    deleteUser,
    dialog,
    editedUser,
    clear,
    saveUser,
    editUser,
    isTable,
    getUsers,
    getUserRoles,
  };
});
