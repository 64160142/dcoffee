import { ref, watch } from "vue";
import { defineStore } from "pinia";

export const useCheckoutStore = defineStore("dialog", () => {
  const dialog = ref(false);
  // watch(dialog, (newDialog) => {
  //   if (!newDialog) {
  //     orderStore.num = 0;
  //   }
  // });
  return { dialog };
});
